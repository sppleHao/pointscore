# PointScore

PointScore is a 3D point cloud network for protein docking model evaluation, which is using protein structures at the atomic level.

## Abstract
protein-protein interactions play important roles in many vital movements. Due to the high cost of experimental methods, computational methods such as protein docking are being developed to predict protein structure. However, there is still a challenge to identify the near-native decoys produced by protein docking. Here, we propose a 3D point cloud method for protein docking model evaluation named PointScore. Pointscore can transform the atomic structure of protein into point cloud representation. Through an intermolecular grouping mechanism and PointMLP block, it can effectively score the protein interface without additional artificial features. Pointscore surpasses the state-of-the-art methods on multiple common datasets and our newly developed antigen-antibody datasets.

## Data Processing
<p align="center">
  <img src="figure/process.png" alt="protocol" width="80%">
</p> 

## Network Architecture

<p align="center">
  <img src="figure/architecture.png" alt="network" width="80%">
</p> 

## Requirements
 - Python 3.6
 - Torch 1.5.1
 - scikit-learn

## Installation  
### 1. [`Install git`](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 
### 2. Clone the repository in your computer 
```
git clone https://gitlab.com/sppleHao/pointscore && cd pointscore
```

### 3. Install Requirements
```
conda create -n PointScore python=3.6
conda activate PointScore
pip install -r requirements.txt 
```

## Usage
```
python3 main.py
  -h, --help            show this help message and exit
  --gpu GPU             Choose gpu id, example: '0,1'(specify use gpu 0 and 1)
  --batch_size          batch_size
  --dropout_rate        dropout_rate
  --data_dir            dataset dir
  --log_dir             log save dir
  --seed                random seed for shuffling
  --fold               specify fold model for prediction
  --checkpoint          model checkpoint save dir
```

### 1 Preprocess Data
```
python preprocess.py --dataset_dir [dataset_dir] --num_workers [num_workers]
```
preprocessing should specify a pdb file with Receptor chain ID 'A' and ligand chain ID 'B'. pdb file must be in a folder named PDB ID. num_workers is used to specify the number of theads to process data.

File Example:
```
dataset dir  
│
└───1A2Y
│   │   1A2Y_01.pdb
│   │   1A2Y_02.pdb
│   │   ...
│   
└───1A2K    
│   │   1A2K_01.pdb
│   │   1A2K_02.pdb
│   │   ...
│
```

### 2 Train Model
```
python main.py --data_dir [data_dir] --gpu=[gpu_id] --batch_size [batch_size] --checkpoint [checkpoint_dir]
```
main.py should specify a file preprocessed by preprocess.py; --gpu is used to specify the gpu id; trianing model will be saved in [checkpoint_dir].

### 2 Evaluate protein complex
```
python eval_ssr.py --gpu=[gpu_id] --fold [fold] --data_dir [data_dir] --sv_dir [sv_dir] 
```
eval_ssr.py should specify the directory that inclues pdb files with Receptor chain ID 'A' and ligand chain ID 'B'; --fold should specify the fold model you will use, where -1 denotes that you want to use the average prediction of 4 fold models and 1,2,3,4 will choose different model for predictions.
The output will be kept in [ssr_sv/{sv_dir}]. The prediction results will be kept in {PDB ID}.txt.   

## Result on Dockground Dataset
<p align="center">
  <img src="figure/result1.png" alt="network" width="80%">
</p> 

## Result on Antibody-662 Dataset
<p align="center">
  <img src="figure/result2.png" alt="network" width="80%">
</p>

## Result on CAPRI Score Set Dataset
<p align="center">
  <img src="figure/result3.png" alt="network" width="80%">
</p>



