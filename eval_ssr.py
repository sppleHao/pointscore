"""
Usage:
python main.py
"""
import sys
import argparse
import numpy as np
import os
import torch
import datetime
import logging
from tqdm import tqdm
import sys
import utils.provider as provider
import importlib
import torch.nn as nn
from torchsampler import ImbalancedDatasetSampler
from sklearn.metrics import roc_auc_score,roc_curve
from utils.early_stopping import EarlyStopping

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = BASE_DIR
sys.path.append(os.path.join(ROOT_DIR, 'model'))
sys.path.append(os.path.join(ROOT_DIR,'utils'))

from utils.data_loader import DataLoaderForComplex, DataLoaderForComplexUseHit,collate_fn,collate_fn_hit
from tensorboardX import SummaryWriter


def parse_args():
    '''PARAMETERS'''
    parser = argparse.ArgumentParser('training')
    parser.add_argument('--batch_size', type=int, default=16, help='batch size in training [default: 24]')
    parser.add_argument('--model', default='point_mlp_group', help='model name [default: pointnet2_msg]')
    parser.add_argument('--gpu', type=str, default='0', help='specify gpu device [default: 0]')
    parser.add_argument('--num_point', type=int, default=1000, help='Point Number [default: 1000]')
    parser.add_argument('--optimizer', type=str, default='Adam', help='optimizer for training [default: Adam]')
    parser.add_argument('--data_dir', type=str, default='capri_1000', help='data root')
    parser.add_argument('--sv_dir', type=str, default='capri_1000', help='data root')
    parser.add_argument('--normal', action='store_true', default=True, help='Whether to use normal information [default: False]')
    parser.add_argument('--fold', type=int, default=2, help='K Fold Number [default: -1]')
    parser.add_argument('--use_ph', type=bool, default=False, help='other features')
    return parser.parse_args()


def main():
    args = parse_args()
    
    '''HYPER PARAMETER'''
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu


    '''DATA LOADING'''
    print('Load dataset ...')
    DATA_PATH = os.path.join(ROOT_DIR,'data')
    DATA_PATH = os.path.join(DATA_PATH,args.data_dir)

    '''MODEL LOADING'''
    MODEL = importlib.import_module(args.model)
    CKPT_PATH = os.path.join(ROOT_DIR,'checkpoint')

    RESULT_PATH = os.path.join(ROOT_DIR,'ssr_sv')
    if not os.path.exists(RESULT_PATH):
        os.mkdir(RESULT_PATH)

    sv_path = os.path.join(RESULT_PATH,args.sv_dir)
    if not os.path.exists(sv_path):
        os.mkdir(sv_path)

    classifiers = []
    if args.fold==-1:
        for fold_id in range(4):
            savepath = os.path.join(CKPT_PATH,'fold{}'.format(fold_id+1),'best_model.pth') 
            model_CKPT = torch.load(savepath)
            '''MODEL LOADING'''
            classifier = MODEL.pointMLP().cuda()
            classifier.load_state_dict(model_CKPT['model_state_dict'])
            classifiers.append(classifier)
    else:
        savepath = os.path.join(CKPT_PATH,'fold{}'.format(args.fold),'best_model.pth') 
        model_CKPT = torch.load(savepath)
        '''MODEL LOADING'''
        classifier = MODEL.pointMLP().cuda()
        classifier.load_state_dict(model_CKPT['model_state_dict'])
        classifiers.append(classifier)

            
    test_complex_names = os.listdir(DATA_PATH)
    test_complex_names = [x for x in test_complex_names if os.path.isdir(os.path.join(DATA_PATH,x))]

    for tcn in test_complex_names:
        COMPlEX_DATA_PATH = os.path.join(DATA_PATH,tcn)
        HITDATA = DataLoaderForComplexUseHit(COMPlEX_DATA_PATH, npoint=1000,use_res=True,use_ph=args.use_ph)
        result_dir = os.path.join(sv_path,'{}.txt'.format(tcn))
        hitDataLoader = torch.utils.data.DataLoader(HITDATA,batch_size=args.batch_size, shuffle=True,collate_fn=collate_fn_hit)
        pred_list = [[] for _ in range(len(classifiers))]
        file_list = []
        with torch.no_grad():
            for j, data in tqdm(enumerate(hitDataLoader), total=len(hitDataLoader)):
                points, target, npoints,ps = data
                points = torch.Tensor(points)
                target = torch.Tensor(target)
                points = points.transpose(2, 1)
                points, target = points.cuda(), target.cuda()
                for index,classifier in enumerate(classifiers):
                    classifier.eval()
                    pred = classifier(points)
                    pred_list[index]+=pred.cpu().numpy().tolist()
                file_list+=ps
        pred_list = np.array(pred_list)
        pred_list = np.mean(pred_list, axis=0)
        file_list = np.array(file_list)
        sort = np.argsort(pred_list)
        with open(result_dir,'w+') as f:
            file_sort = file_list[sort][::-1]
            pred_sort = pred_list[sort][::-1]
            for i in range(pred_list.shape[0]):
                file_name = str(file_sort[i]).split(os.sep)[-1]
                no = file_name.split('.')[1]
                f.write('{}'.format(file_name)+'\t'+str(pred_sort[i])+'\n')

if __name__ == '__main__':
    main()
