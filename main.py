"""
Usage:
python main.py
"""
import sys
import argparse
import numpy as np
import os
import torch
import datetime
import logging
from tqdm import tqdm
import sys
import utils.provider as provider
import importlib
import torch.nn as nn
from torchsampler import ImbalancedDatasetSampler
from sklearn.metrics import roc_auc_score,roc_curve
from utils.early_stopping import EarlyStopping

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = BASE_DIR
sys.path.append(os.path.join(ROOT_DIR, 'model'))
sys.path.append(os.path.join(ROOT_DIR,'utils'))

from utils.data_loader import DataLoaderForComplex, DataLoaderForComplexUseHit,collate_fn,collate_fn_hit
from tensorboardX import SummaryWriter


def parse_args():
    '''PARAMETERS'''
    parser = argparse.ArgumentParser('training')
    parser.add_argument('--batch_size', type=int, default=16, help='batch size in training [default: 24]')
    parser.add_argument('--model', default='point_mlp_group', help='model name [default: pointnet2_msg]')
    parser.add_argument('--epoch',  default=100, type=int, help='number of epoch in training [default: 100]')
    parser.add_argument('--learning_rate', default=0.001, type=float, help='learning rate in training [default: 0.001]')
    parser.add_argument('--gpu', type=str, default='0', help='specify gpu device [default: 0]')
    parser.add_argument('--num_point', type=int, default=1000, help='Point Number [default: 1000]')
    parser.add_argument('--optimizer', type=str, default='Adam', help='optimizer for training [default: Adam]')
    parser.add_argument('--data_dir', type=str, default='dockground61_1000', help='data root')
    parser.add_argument('--log_dir', type=str, default='dockground61_1000_group_v', help='out root')
    parser.add_argument('--decay_rate', type=float, default=1e-4, help='decay rate [default: 1e-4]')
    parser.add_argument('--normal', action='store_true', default=True, help='Whether to use normal information [default: False]')
    parser.add_argument('--kfold', type=int, default=4, help='K Fold Number [default: 5]')
    parser.add_argument('--use_ph', type=bool, default=False, help='other features')
    parser.add_argument('--seed', type=int,default=1024,help='random seed')
    parser.add_argument('-c', '--checkpoint', type=str, default='checkpoint',help='path to save checkpoint (default: checkpoint)')
    return parser.parse_args()


def main():
    args = parse_args()

    def log_string(str):
        logger.info(str)
        print(str)

    '''HYPER PARAMETER'''
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    if args.seed is not None:
        torch.manual_seed(args.seed)
        np.random.seed(args.seed)
        torch.cuda.manual_seed_all(args.seed)
        torch.cuda.manual_seed(args.seed)
        torch.set_printoptions(10)
        torch.backends.cudnn.benchmark = False
        torch.backends.cudnn.deterministic = True
        os.environ['PYTHONHASHSEED'] = str(args.seed)

    '''CREATE DIR'''
    timestr = str(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M'))
    log_dir_root = os.path.join(ROOT_DIR,'log/')
    log_dir_root.mkdir(exist_ok=True)
    log_dir_root = log_dir_root.joinpath('classification_kfold')
    log_dir_root.mkdir(exist_ok=True)
    if args.log_dir is None:
        log_dir_root = log_dir_root.joinpath(timestr)
    else:
        log_dir_root = log_dir_root.joinpath(args.log_dir)
    log_dir_root.mkdir(exist_ok=True)
    '''TensorboardX'''
    writer = SummaryWriter()

    '''DATA LOADING'''
    log_string('Load dataset ...')
    DATA_PATH = os.path.join(ROOT_DIR,'data')
    DATA_PATH = os.path.join(DATA_PATH,args.data_dir)
    
    #kfold split
    SET_TXT_DIR = os.path.join(DATA_PATH,'set.txt')
    DATASET = DataLoaderForComplex(root=DATA_PATH, npoint=args.num_point,normal_channel=args.normal,use_ph=args.use_ph)

    '''MODEL LOADING'''
    MODEL = importlib.import_module(args.model)
    CKPT_PATH = os.path.join(ROOT_DIR,args.checkpoint)


    k_folder_num = args.kfold
    for fold_id in [2,6,10,14]:

        fold = fold_id%k_folder_num

        patience = 5
        early_stopping = EarlyStopping(patience, verbose=True)
    
        experiment_dir = log_dir_root.joinpath('kfold{}'.format(fold_id+1))
        experiment_dir.mkdir(exist_ok=True)
        log_dir = experiment_dir.joinpath('logs/')
        log_dir.mkdir(exist_ok=True)

        '''LOG'''
        args = parse_args()
        logger = logging.getLogger("Model")
        logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler = logging.FileHandler('%s/%s.txt' % (log_dir, args.model))
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        log_string('PARAMETER ...')
        log_string(args)

        '''DATASET LOADING'''
        log_string('==> Building CV Dataset..')
        TRAIN_DATASET ,VALID_DATASET ,TEST_DATASET = DATASET.get_cv_dataloader_use_txt(SET_TXT_DIR,fold)
        trainDataLoader = torch.utils.data.DataLoader(TRAIN_DATASET,sampler=ImbalancedDatasetSampler(TRAIN_DATASET),batch_size=args.batch_size, shuffle=False, num_workers=4, drop_last=True, collate_fn=collate_fn)
        validDataLoader = torch.utils.data.DataLoader(VALID_DATASET,sampler=ImbalancedDatasetSampler(VALID_DATASET),batch_size=args.batch_size, shuffle=False, num_workers=4 ,drop_last=True, collate_fn=collate_fn)
        testDataLoader = torch.utils.data.DataLoader(TEST_DATASET, batch_size=args.batch_size, shuffle=False, num_workers=4,drop_last=True,collate_fn=collate_fn)
        test_complex_names = TEST_DATASET.get_complex_names()
        print(test_complex_names)


        '''Model'''
        log_string('==> Building model..')
        classifier = MODEL.pointMLP().cuda()
        criterion = nn.BCELoss()
        #criterion = nn.CrossEntropyLoss().cuda()
    
        
        checkpoints_dir = os.path.join(CKPT_PATH,'/kfold{}'.format(fold_id+1))
        if not os.path.exists(checkpoints_dir):
            os.mkdir(checkpoints_dir)
        try:
            checkpoint = torch.load(os.path.join(checkpoints_dir,'best_model.pth'))
            start_epoch = checkpoint['epoch']
            classifier.load_state_dict(checkpoint['model_state_dict'])
            log_string('Use pretrain model')
        except:
            log_string('No existing model, starting training from scratch...')
            start_epoch = 0

        if args.optimizer == 'Adam':
            optimizer = torch.optim.Adam(
                classifier.parameters(),
                lr=args.learning_rate,
                betas=(0.9, 0.999),
                eps=1e-08,
                weight_decay=args.decay_rate
            )
        else:
            optimizer = torch.optim.SGD(classifier.parameters(), lr=0.01, momentum=0.9)

        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.8)

        global_epoch = 0
        global_step = 0
        best_instance_acc = 0.0
        best_class_acc = 0.0

        train_size = TRAIN_DATASET.get_size()
        valid_size = VALID_DATASET.get_size()
        test_size = TEST_DATASET.get_size()

        '''Training'''
        logger.info('Start training...')
        for epoch in range(start_epoch,args.epoch):

            log_string('Epoch %d (%d/%s):' % (global_epoch + 1, epoch + 1, args.epoch))

            train_aucs = []
            mean_correct = []
            running_loss = 0.0
            scheduler.step()
            for batch_id, data in tqdm(enumerate(trainDataLoader, 0), total=len(trainDataLoader), smoothing=0.9):
                points, target, npoint = data
                points = provider.random_point_dropout(points)
                # points[:,:, 0:3] = provider.random_scale_point_cloud(points[:,:, 0:3])
                points[:,:, 0:3] = provider.shift_point_cloud(points[:,:, 0:3])


                points = torch.Tensor(points)
                target = torch.Tensor(target)
                points = points.transpose(2, 1)
                points, target = points.cuda(), target.cuda()
                optimizer.zero_grad()

                classifier = classifier.train()
                pred = classifier(points)
                loss = criterion(pred, target)

                zero = torch.zeros_like(pred)
                one = torch.ones_like(pred)
                pred_choice = torch.where(pred > 0.5, one, zero)
                correct = pred_choice.eq(target.long().data).cpu().sum()
                mean_correct.append(correct.item() / float(points.size()[0]))

                running_loss+= loss.item()
                loss.backward()
                optimizer.step()
                global_step += 1

            train_instance_acc = np.mean(mean_correct)
            log_string('Train Instance Accuracy: %f' % train_instance_acc)


            running_loss = running_loss / train_size
            writer.add_scalar('train_loss',running_loss,global_step=epoch)
            writer.add_scalar('train_acc',train_instance_acc,global_step=epoch)

            #Valid
            with torch.no_grad():
                mean_correct = []
                running_loss = 0.0
                class_acc = np.zeros((2,3))
                for j, data in tqdm(enumerate(validDataLoader), total=len(validDataLoader)):
                    points, target, npoint = data
                    points = torch.Tensor(points)
                    target = torch.Tensor(target)
                
                    points = points.transpose(2, 1)
                    points, target= points.cuda(), target.cuda()
                    classifier = classifier.eval()
                    pred = classifier(points)
                    loss = criterion(pred, target)

                    zero = torch.zeros_like(pred)
                    one = torch.ones_like(pred)
                    pred_choice = torch.where(pred > 0.5, one, zero)
                    for cat in np.unique(target.cpu()):
                        classacc = pred_choice[target==cat].eq(target[target==cat].long().data).cpu().sum()
                        cat = int(cat)
                        class_acc[cat,0]+= classacc.item()
                        class_acc[cat,1]+= points[target==cat].size()[0]
                    
                    correct = pred_choice.eq(target.long().data).cpu().sum()
                    mean_correct.append(correct.item()/float(points.size()[0]))
                    running_loss+= loss.item()
            

                class_acc[:,2] =  class_acc[:,0]/ class_acc[:,1]
                class_acc = np.mean(class_acc[:,2])
                valid_instance_acc = np.mean(mean_correct)
                running_loss = running_loss / valid_size

                writer.add_scalar('valid_loss',running_loss,global_step=epoch)
                writer.add_scalar('valid_acc',valid_instance_acc,global_step=epoch)

                log_string('Valid Instance Accuracy: %f, Class Accuracy: %f'% (valid_instance_acc, class_acc))
                log_string('Best Instance Accuracy: %f, Class Accuracy: %f'% (best_instance_acc, best_class_acc))

                if (class_acc >= best_class_acc and epoch>5):
                    best_instance_acc = valid_instance_acc
                    best_class_acc = class_acc
                    best_epoch = epoch + 1
                    logger.info('Save model...')
                    savepath = os.path.join(checkpoints_dir,'best_model.pth')
                    log_string('Saving at %s'% savepath)
                    state = {
                        'epoch': best_epoch,
                        'instance_acc': valid_instance_acc,
                        'class_acc': class_acc,
                        'model_state_dict': classifier.state_dict(),
                        'optimizer_state_dict': optimizer.state_dict(),
                    }
                    torch.save(state, savepath)
                global_epoch += 1

                savepath = os.path.join(checkpoints_dir,'last_model.pth')
                log_string('Saving at %s'% savepath)
                state = {
                    'epoch': epoch,
                    'instance_acc': valid_instance_acc,
                    'class_acc': class_acc,
                    'model_state_dict': classifier.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    }
                torch.save(state, savepath)

                if (epoch>5):
                    early_stopping(1-class_acc, classifier)                

                if early_stopping.early_stop:
                    print('Early stop')
                    break
        

        '''Testing'''
        savepath = os.path.join(checkpoints_dir,'best_model.pth') 
        model_CKPT = torch.load(savepath)
        classifier = MODEL.pointMLP().cuda()
        criterion = nn.BCELoss()
        classifier.load_state_dict(model_CKPT['model_state_dict'])

        y_true = []
        y_pred = []
        classifier.eval()
        aucs = []
        with torch.no_grad():
            mean_correct = []
            class_acc = np.zeros((2,3))
            for j, data in tqdm(enumerate(testDataLoader), total=len(testDataLoader)):
                points, target, npoints = data

                points = torch.Tensor(points)
                target = torch.Tensor(target)


                y_true.append(target)
                points = points.transpose(2, 1)
                points, target = points.cuda(), target.cuda()
                pred = classifier(points)
                loss = criterion(pred, target)
                zero = torch.zeros_like(pred)
                one = torch.ones_like(pred)
                pred_choice = torch.where(pred > 0.5, one, zero)
                y_pred.append(pred_choice.cpu().numpy())
                    
                for cat in np.unique(target.cpu()):
                    classacc = pred_choice[target==cat].eq(target[target==cat].long().data).cpu().sum()
                    cat = int(cat)
                    class_acc[cat,0]+= classacc.item()
                    class_acc[cat,1]+= points[target==cat].size()[0]
                correct = pred_choice.eq(target.long().data).cpu().sum()
                mean_correct.append(correct.item()/float(points.size()[0]))
                    
            np.set_printoptions(suppress=True)
            class_acc[:,2] =  class_acc[:,0]/ class_acc[:,1]
            print(class_acc)
            class_acc = np.mean(class_acc[:,2])
            test_instance_acc = np.mean(mean_correct)
            print('%s Instance Accuracy: %f, Class Accurancy(AUC): %f' % ('test',test_instance_acc,class_acc))
            log_string('Test Instance Accuracy: %f, Class Accuracy: %f'% (test_instance_acc, class_acc))
            y_true = np.concatenate(y_true)
            y_pred = np.concatenate(y_pred)
                
            fpr_score = [0.005,0.01,0.02,0.05]
            sk_auc = roc_auc_score(y_true=y_true,y_score=y_pred)
            fpr,tpr,thersholds = roc_curve(y_true,y_pred)
        

        '''Hit Result Generate'''
        test_complex_names = TEST_DATASET.get_complex_names()
        savepath = os.path.join(checkpoints_dir,'best_model.pth') 
        model_CKPT = torch.load(savepath)
        '''MODEL LOADING'''
        num_class = 1
        classifier = MODEL.pointMLP().cuda()
        criterion = nn.BCELoss()
        classifier.load_state_dict(model_CKPT['model_state_dict'])
        result_folder = os.path.join(experiment_dir,'hit_result')
        if not os.path.exists(result_folder):
            os.mkdir(result_folder)

        for tcn in test_complex_names:
            COMPlEX_DATA_PATH = os.path.join(DATA_PATH,tcn)
            HITDATA = DataLoaderForComplexUseHit(COMPlEX_DATA_PATH, npoint=1000,use_res=True,use_ph=args.use_ph)
            result_dir = os.path.join(result_folder,'{}.txt'.format(tcn))
            hitDataLoader = torch.utils.data.DataLoader(HITDATA,batch_size=4, shuffle=True,collate_fn=collate_fn_hit)
            preds = []
            file_list = []
            classifier.eval()
            with torch.no_grad():
                for j, data in tqdm(enumerate(hitDataLoader), total=len(hitDataLoader)):
                    points, target, npoints,ps = data

                    points = torch.Tensor(points)
                    target = torch.Tensor(target)
                            #target = target[:, 0]
                    points = points.transpose(2, 1)
                    points, target = points.cuda(), target.cuda()
                            #pred, _ = classifier(points)
                    pred = classifier(points)
                    preds+=pred.cpu().numpy().tolist()
                    file_list+=ps
            preds = np.array(preds)
            file_list = np.array(file_list)
            sort = np.argsort(preds)
            with open(result_dir,'w+') as f:
                file_sort = file_list[sort][::-1]
                pred_sort = preds[sort][::-1]
                for i in range(preds.shape[0]):
                    file_name = str(file_sort[i]).split(os.sep)[-1]
                    no = file_name.split('.')[1]
                    f.write('{}'.format(file_name)+'\t'+str(pred_sort[i])+'\n')


    logger.info('End of training...')

if __name__ == '__main__':
    args = parse_args()
    main(args)
