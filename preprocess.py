import os
from re import L
import sys
import shutil
from os import mkdir, name
import numpy as np
import operator
from functools import reduce
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
import importlib
from multiprocessing import Pool
from multiprocessing import cpu_count    # 查看cpu核心数
from tqdm import trange 

#是否使用固定原子数和固定原子总数
use_need = False
if_N = False
N = 1000
processor = 4
'''
对文件夹下所有pdb进行界面提取
'''
#存放pdb的文件夹路径
#input_dir = 'C:\\Users\\cxhrzh\\Desktop\\code\\final_out_AB
#输出文件夹的路径,其中txt目录存放编码，pdb文件目录存放界面pdb
#output_dir = 'C:\\Users\\cxhrzh\\Desktop\\code\\out_1000'

need_path = 'C:\\Users\\cxhrzh\\Desktop\\code\\need.txt'
input_dir = 'C:\\Users\\cxhrzh\\Desktop\\code\\dockground_origin'
output_dir = 'C:\\Users\\cxhrzh\\Desktop\\code\\dockground_origin_1000'

def get_atom_list_from_pdb(chain,chain_type):
    choice = {'R':'receptor','L':'ligand'}
    ATOM_TYPE = ['C','N','O']
    atom_list = []
    for atomID in chain.atomID.unique():
        atom = chain[chain.atomID==atomID]
        _atom= atom.values
        #x, y, z = [ float(x) for x in [atom.x,atom.y,atom.z]]
        atom_name = str(_atom[0,2]).strip()
        resName = str(_atom[0,3]).strip()
        resID = str(_atom[0,5]).strip()
        x = float(str(_atom[0,6]).strip())
        y = float(str(_atom[0,7]).strip())
        z = float(str(_atom[0,8]).strip())
        #print(x,y,z)
        if atom_name not in ATOM_TYPE:
            atom_name = 'Others'
        atom_tuple = [x,y,z,atom_name,resName,resID,chain_type,atomID]
        atom_list.append(atom_tuple)
    
    print('in {} has {} atoms'.format(choice[chain_type],len(atom_list)))
    return atom_list

def get_atom_of_res_from_pdb(chain,chain_type):
    choice = {'R':'receptor','L':'ligand'}
    atom_num = 0
    ATOM_TYPE = ['C','N','O']
    res_list =[]
    for resID in chain.resID.unique():
        atom_list = []
        t= chain.resID==resID
        res = chain[t]
        #print(res)
        resName = chain[t].resName.unique()[0]
        for index,atom in res.iterrows():
            x,y,z = [ float(x) for x in [atom.x,atom.y,atom.z]]
            atom_name = atom.atomName.strip()
            #print(atom_name)
            #print(len(atom_name))
            if atom_name not in ATOM_TYPE:
                atom_name = 'Others'
            atom_tuple = [x,y,z,atom_name,resName,resID,chain_type]
            atom_list.append(atom_tuple)
            atom_num= atom_num+ len(atom_list)
        res_list.append(atom_list)
    print('in {} has {} atoms'.format(choice[chain_type],atom_num))
    return res_list

def get_interface_from_atom_N(atom_1_list,atom_2_list, N=500):
    r_atom_list_ID =[]
    l_atom_list_ID =[]
    
    distances = []
    for r_ID , r_atom in enumerate(atom_1_list):
        for l_ID , l_atom in enumerate(atom_2_list):
            point_a= np.array(r_atom[0:3])
            point_b = np.array(l_atom[0:3])
            distance = np.sqrt(np.sum(np.square(point_a-point_b)))
            distances.append([r_ID,l_ID,distance])
    
    distances = np.array(distances)
    sort_distances = distances[np.argsort(distances[:,2])].tolist()

    num = 0
    for d in sort_distances:
        r_ID, l_ID, distance = d
        r_ID = int(r_ID)
        l_ID = int(l_ID)
        if (r_ID not in r_atom_list_ID) and (l_ID not in l_atom_list_ID):
            r_atom_list_ID.append(r_ID)
            l_atom_list_ID.append(l_ID)
            num = num+1
        if num == N:
            break

    r_list = []
    l_list = []

    for index in r_atom_list_ID:
        r_atom = atom_1_list[index]
        r_list.append(r_atom)
    for index in l_atom_list_ID:
        l_atom = atom_2_list[index]
        l_list.append(l_atom)

    print('after cutoff receptor has {} atoms'.format(len(r_list)))
    print('after cutoff ligand has {} atoms'.format(len(l_list)))
    return r_list,l_list
    
def get_interface_from_atom(res_1_list,res_2_list, radius=10):
    r_res_list_ID =[]
    l_res_list_ID =[]
    
    for r_ID , receptor_res in enumerate(res_1_list):
        for l_ID , ligand_res in enumerate(res_2_list):
            goon = False
            for r_atom in receptor_res:
                if(goon):
                    break
                for l_atom in ligand_res:
                    point_a= np.array(r_atom[0:3])
                    point_b = np.array(l_atom[0:3])
                    distance = np.sqrt(np.sum(np.square(point_a-point_b)))
                    if distance<radius:
                        if r_ID not in r_res_list_ID:
                            r_res_list_ID.append(r_ID)
                        if l_ID not in l_res_list_ID:
                            l_res_list_ID.append(l_ID)
                        goon =True
                        break
    r_list = []
    l_list = []
    r_num = 0
    l_num = 0
    
    for index in r_res_list_ID:
        r_res = res_1_list[index]
        r_list.append(r_res)
        r_num= r_num + len(r_res)
    for index in l_res_list_ID:
        l_res = res_2_list[index]
        l_list.append(l_res)
        l_num = l_num+ len(l_res)
        
    print('after cutoff receptor has {} atoms'.format(r_num))
    print('after cutoff ligand has {} atoms'.format(l_num))
    
    return r_list,l_list

def encode_res(new_data, enc, pca=None):
    new_data = np.array(new_data).reshape(len(new_data), -1)
    new_data = enc.transform(new_data).toarray()
    if pca:
        new_data = pca.transform(new_data)
    return new_data

def encode_atom_list(r_list,l_list,is_atom_list=False):
    
    res_list = []

    if is_atom_list:
        res_list = r_list + l_list
    else:
        res_list = reduce(operator.add, r_list) + reduce(operator.add, l_list)
    
    atom_names_origin = ['C','N','O','Others']
    atom_names = np.array(atom_names_origin).reshape(len(atom_names_origin), -1)
    
    res_names_origin = ['ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL']
    res_names = np.array(res_names_origin).reshape(len(res_names_origin), -1)

    #One Hot
    res_enc = OneHotEncoder()
    res_enc.fit(res_names)
    
    atom_enc = OneHotEncoder()
    atom_enc.fit(atom_names)
    
    #Encode
    new_list =[]
    for row in res_list:
        xyz = np.array(row[0:3])
        resName = encode_res([row[4]],res_enc)[0]
        atomName =encode_res([row[3]],atom_enc)[0]
        chain = row[-1]
        if chain=="R":
            chain = np.array([1,0])
        else:
            chain = np.array([0,1])
        new_row = np.concatenate([xyz,atomName,resName,chain])
        new_list.append(new_row)
    return new_list

def process_pdb_file_by_atom_N(pdb_file,sv_pdb_file,chian_id_1,chian_id_2,N=1000):
    '''
    :param: pdb_file <str>
    :return:  interface <DataFrame>
    '''
    RES_TYPE_ORIGIN = ['ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL']
    ATOM_TYPE_ORIGIN = ['C','N','O','Others']
    #Open a pdb file
    metas =[]
    with open(pdb_file,'r') as f:
        line = f.readline().strip()
        while line:
            if line[0:4] =="ATOM":
                metas.append([line[0:4],line[7:11],line[12:16],line[17:20],line[21],line[23:26],line[30:38],line[38:46],line[46:54],line[55:60],line[60:66]])
            line = f.readline().strip()
    a = np.array([np.array(x) for x in metas])
    
    #To DataFrame
    columns = ["atom","atomID","atomName","resName","chainID","resID","x","y","z","occupancy","tempFactor"]
    data = pd.DataFrame(a,columns=columns)
    data = data[data.resName.isin(RES_TYPE_ORIGIN)].copy()
    
    #chain_list = data.chainID.unique()
    
    chain_1 = data[data.chainID.isin(chian_id_1)].copy()
    chain_2 = data[data.chainID.isin(chian_id_2)].copy()

    atom_1_list = get_atom_list_from_pdb(chain_1, "R")
    atom_2_list = get_atom_list_from_pdb(chain_2, "L")

    r_list , l_list=get_interface_from_atom_N(atom_1_list,atom_2_list,N//2)

    r_atomIDs = []
    l_atomIDs = []

    for atom in (r_list):
        r_atomIDs.append(atom[-1])
    for atom in l_list:
        l_atomIDs.append(atom[-1])

    with open(sv_pdb_file,'w+') as wf:
        with open(pdb_file,'r') as rf:
            line = rf.readline().strip()
            while line:
                if line[0:4] == "ATOM":
                    if line[21] == 'A':
                        if line[7:11] in r_atomIDs:
                            wf.write(line+'\n')
                    elif line[21] == 'B':
                        if line[7:11] in l_atomIDs:
                            wf.write(line+'\n')
                else:
                    wf.write(line+'\n')
                line = rf.readline().strip()
    
    e_list = encode_atom_list(r_list,l_list,is_atom_list=True)

    return e_list

def process_pdb_file_by_atom(pdb_file,sv_pdb_file,chian_id_1,chian_id_2,radius=10):
    '''
    :param: pdb_file <str>
    :return:  interface <DataFrame>
    '''
    RES_TYPE_ORIGIN = ['ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL']
    ATOM_TYPE_ORIGIN = ['C','N','O','Others']
    #Open a pdb file
    metas =[]
    with open(os.path.join(pdb_file)) as f:
        line = f.readline().strip()
        while line:
            if line[0:4] =="ATOM":
                metas.append([line[0:4],line[7:11],line[12:16],line[17:20],line[21],line[23:26],line[30:38],line[38:46],line[46:54],line[55:60],line[60:66]])
            line = f.readline().strip()
    a = np.array([np.array(x) for x in metas])
    
    #To DataFrame
    columns = ["atom","atomID","atomName","resName","chainID","resID","x","y","z","occupancy","tempFactor"]
    data = pd.DataFrame(a,columns=columns)
    data = data[data.resName.isin(RES_TYPE_ORIGIN)].copy()
    
    #chain_list = data.chainID.unique()
    
    chain_1 = data[data.chainID.isin(chian_id_1)].copy()
    chain_2 = data[data.chainID.isin(chian_id_2)].copy()

    res_1_list = get_atom_of_res_from_pdb(chain_1, "R")
    res_2_list = get_atom_of_res_from_pdb(chain_2, "L")

    r_list , l_list=get_interface_from_atom(res_1_list,res_2_list,radius=radius)

    resIDs = []

    for res in (r_list+l_list):
        resIDs.append(res[0][5])
 
    with open(sv_pdb_file,'w+') as wf:
        with open(pdb_file,'r') as rf:
            line = rf.readline().strip()
            while line:
                if line[0:4] == "ATOM":
                    if line[23:26] in resIDs:
                        wf.write(line+'\n')
                else:
                    wf.write(line+'\n')
                line = rf.readline().strip()


    e_list = encode_atom_list(r_list,l_list)

    return e_list

def interface_nearest_point_sample(point,npoint):
    R= point[point[:,-1]==0]
    L= point[point[:,-1]==1]

    num_r = R.shape[0]
    num_l = L.shape[0]

    distances = []
    dicts = []
    index_r = []
    index_l = []
    
    def euclidean(x, y):
        return np.sqrt(np.sum((x - y)**2))

    for i in range(num_r):
        r = R[i]
        for j in range(num_l):
            l = L[j]
            d = euclidean(r[:3],l[:3])
            distances.append(d)
            dicts.append([i,j,d])
    
    dicts = np.array(dicts)

    dicts = dicts[dicts[:,2].argsort()]

    for i in range(dicts.shape[0]):
        r = dicts[i,0]
        l = dicts[i,1]
        if (r not in index_r) and len(index_r) < npoint//2:
            index_r.append(r)
        if (l not in index_l) and len(index_l) < npoint//2:
            index_l.append(l)
    
    index_r = [int(x) for x in index_r]
    index_l = [int(x) for x in index_l]
    
    receptor = R[index_r]
    ligand = L[index_l]
    point = np.concatenate((receptor,ligand), axis=0)

    return point

def preprocess(input_path,txt_sv_path,pdb_sv_path):
    if not os.path.exists(txt_sv_path):
        os.mkdir(txt_sv_path)
    if not os.path.exists(pdb_sv_path):
        os.mkdir(pdb_sv_path)

    listfiles=[x for x in os.listdir(input_path) if ".pdb" in x]
    for item in listfiles:
        input_pdb_path=os.path.join(input_path,item)
        output_pdb_path = os.path.join(pdb_sv_path,item)
        encode_interface_list = process_pdb_file_by_atom(input_pdb_path,output_pdb_path,['A'],['B'])
        point = np.array(encode_interface_list)
        np.savetxt(os.path.join(txt_sv_path,item[:-4]+'.txt'),point,delimiter=' ')

def preprocess_N(input_path,txt_sv_path,pdb_sv_path):
    if not os.path.exists(txt_sv_path):
        os.mkdir(txt_sv_path)
    if not os.path.exists(pdb_sv_path):
        os.mkdir(pdb_sv_path)

    listfiles=[x for x in os.listdir(input_path) if ".pdb" in x]
    for item in listfiles:
        input_pdb_path=os.path.join(input_path,item)
        output_pdb_path = os.path.join(pdb_sv_path,item)
        encode_interface_list = process_pdb_file_by_atom_N(input_pdb_path,output_pdb_path,['A'],['B'],N)
        point = np.array(encode_interface_list)
        np.savetxt(os.path.join(txt_sv_path,item[:-4]+'.txt'),point,delimiter=' ')
    
def single_worker(pdb_list,input_dir,output_dir,if_N):
     for i in trange(len(pdb_list)):
        pdb_name = pdb_list[i]
        input_path =  os.path.join(input_dir,pdb_name)
        txt_output_path = os.path.join(output_dir,'txt',pdb_name)
        pdb_output_path = os.path.join(output_dir,'pdb',pdb_name)
        if if_N:
            preprocess_N(input_path,txt_output_path,pdb_output_path)
        else:
            preprocess(input_path,txt_output_path,pdb_output_path)


if __name__ == '__main__':
    txt_output_path = os.path.join(output_dir,'txt')
    pdb_output_path = os.path.join(output_dir,'pdb')
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    if not os.path.exists(txt_output_path):
        os.mkdir(txt_output_path)
    if not os.path.exists(pdb_output_path):
        os.mkdir(pdb_output_path)

    
    pdb_list = os.listdir(input_dir)
    if use_need:
        pdb_list = []
        with open(need_path,'r') as f:
            pdb_list=f.readlines()
            pdb_list = [x.strip() for x in pdb_list]
    #print(pdb_list)
    num_cores = cpu_count()  
    num_pdb = len(pdb_list)
    # single_worker(['2NYY'],input_dir,output_dir,if_N)
    p = Pool(processor)
    print(num_pdb)
    n = num_pdb//processor +1
    for i in range(processor):
        start = n*i
        end = num_pdb if i==processor-1 else n*(i+1)
        pdb_sub_list = pdb_list[start:end]
        #print(start,end)
        #print(pdb_sub_list)
        p.apply_async(single_worker,args=(pdb_sub_list, input_dir, output_dir,if_N))

    p.close()
    p.join()

    '''

    
    input_path = 'D:\\share\\PPI4DOCK\\final_out_AB\\1a4y_CD'
    txt_output_path = os.path.join('D:\\share\\PPI4DOCK\\10A','txt')
    pdb_output_path = os.path.join('D:\\share\\PPI4DOCK\\10A','pdb')
    preprocess_N(input_path,txt_output_path,pdb_output_path)
    '''
    
    '''
    input_path = 'C:\\Users\\cxhrz\\Desktop\\code\\Dock\\decoy\\decoys_AB\\'
    output_path = os.path.join('C:\\Users\\cxhrz\\Desktop\\code\\Dock\\decoy\\','hit')
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    groups = os.listdir(input_path)
    for i in range(4):
        group_path = os.path.join(input_path,groups[i])
        pdb = os.listdir(group_path)
        for pdb_name in pdb:
            in_path = os.path.join(group_path,pdb_name)
            new_g_path =os.path.join(output_path,groups[i])
            if not os.path.exists(new_g_path):
                os.mkdir(new_g_path)
            sv_path = os.path.join(new_g_path,pdb_name)
            predict_multi(in_path,sv_path)
    '''