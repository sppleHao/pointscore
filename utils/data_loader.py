"""
author:hao
date:16/10/2020

point cloud data preprocessing
"""

from traceback import print_list
from numpy.core.function_base import add_newdoc
import pandas as pd
import numpy as np
import warnings
import os
from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split
warnings.filterwarnings('ignore')



def pc_normalize(pc):
    """
    Normalize coords for all sample points 
    """
    centroid = np.mean(pc, axis=0)
    pc = pc - centroid
    m = np.max(np.sqrt(np.sum(pc**2, axis=1)))
    pc = pc / m
    return pc

def interface_nearest_point_sample(point,npoint):
    R= point[point[:,-1]==0]
    L= point[point[:,-1]==1]

    num_r = R.shape[0]
    num_l = L.shape[0]

    distances = []
    dicts = []
    index_r = []
    index_l = []
    
    def euclidean(x, y):
        return np.sqrt(np.sum((x - y)**2))

    for i in range(num_r):
        r = R[i]
        for j in range(num_l):
            l = L[j]
            d = euclidean(r[:3],l[:3])
            distances.append(d)
            dicts.append([i,j,d])
    
    dicts = np.array(dicts)

    dicts = dicts[dicts[:,2].argsort()]

    for i in range(dicts.shape[0]):
        r = dicts[i,0]
        l = dicts[i,1]
        if (r not in index_r) and len(index_r) < npoint//2:
            index_r.append(r)
        if (l not in index_l) and len(index_l) < npoint//2:
            index_l.append(l)
    
    indexs = index_r+index_l
    indexs = [int(x) for x in indexs]

    point = point[indexs]

    return point

def farthest_point_sample(point, npoint):
    """
    Input:
        xyz: pointcloud data, [N, D]
        npoint: number of samples
    Return:
        centroids: sampled pointcloud index, [npoint, D]
    """
    N, D = point.shape
    xyz = point[:,:3]
    centroids = np.zeros((npoint,))
    distance = np.ones((N,)) * 1e10
    farthest = np.random.randint(0, N)
    for i in range(npoint):
        centroids[i] = farthest
        centroid = xyz[farthest, :]
        dist = np.sum((xyz - centroid) ** 2, -1)
        mask = dist < distance
        distance[mask] = dist[mask]
        farthest = np.argmax(distance, -1)
    point = point[centroids.astype(np.int32)]
    return point

class PPI4DOCKDataLoader_ForHit(Dataset):
    def __init__(self, root,  npoint=50, split='hit_test', uniform=False, normal_channel=True, cache_size=15000):
        self.root = root
        self.npoints = npoint
        self.uniform = uniform
        self.catfile = os.path.join(self.root, 'class.txt')

        self.cat = [line.rstrip() for line in open(self.catfile)]
        self.classes = dict(zip(self.cat, range(len(self.cat))))
        self.normal_channel = normal_channel

        assert (split == 'hit_test')
        self.datapath = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'hit_test.txt'))]
        print('The size of %s data is %d'%(split,len(self.datapath)))

        self.cache_size = cache_size  # how many data points to cache in memory
        self.cache = {}  # from index to (point_set, cls) tuple
    
    def get_size(self):
        return len(self.datapath)

    def __len__(self):
        return len(self.datapath)

    def _get_item(self, index):
        if index in self.cache:
            point_set, label = self.cache[index]
        else:
            path = self.datapath[index]
            if path.endswith('origin.txt'):
                label = self.classes['postive']
            else:
                label = self.classes['negtive']
            point_set = np.loadtxt(path, delimiter=' ').astype(np.float32)

            if self.uniform:
                point_set = farthest_point_sample(point_set, self.npoints)
            else:
                point_set = point_set[0:self.npoints,:]

            point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

            if not self.normal_channel:
                point_set = point_set[:, 0:3]

            if len(self.cache) < self.cache_size:
                self.cache[index] = (point_set, label)

        return point_set, label

    def __getitem__(self, index):
        return self._get_item(index)
class PPI4DOCKDataLoader(Dataset):
    def __init__(self, root,  npoint=720, split='train', uniform=False, normal_channel=True, cache_size=15000,use_ph=False):
        self.root = root
        self.npoints = npoint
        self.uniform = uniform
        self.catfile = os.path.join(self.root, 'class.txt')
        self.use_ph =use_ph
        if self.use_ph:
            ph_table = pd.read_csv(os.path.join(self.root, 'physicochemical_class_onehot.csv'),index_col=0)
            self.ph_table = ph_table.values

        self.cat = [line.rstrip() for line in open(self.catfile)]
        self.classes = dict(zip(self.cat, range(len(self.cat))))
        #print(self.classes['postive'])
        self.normal_channel = normal_channel

        data_paths = {}
        #data_paths['train'] = [line.rstrip() for line in open(os.path.join(self.root, 'train.txt'))]
        #data_paths['valid'] = [line.rstrip() for line in open(os.path.join(self.root, 'valid.txt'))]
        #data_paths['test'] = [line.rstrip() for line in open(os.path.join(self.root, 'test.txt'))]

        data_paths['train'] = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'train.txt'))]
        data_paths['valid'] = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'valid.txt'))]
        data_paths['test'] = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'test.txt'))]


        assert (split == 'train' or split == 'valid' or split == 'test')
        #shape_names = ['_'.join(x.split('_')[0:-1]) for x in shape_ids[split]]
        # list of (shape_name, shape_txt_file_path) tuple
        #self.datapath = [(shape_names[i], os.path.join(self.root, shape_names[i], shape_ids[split][i]) + '.txt') for i
                         #in range(len(shape_ids[split]))]
        self.datapath = data_paths[split]
        print('The size of %s data is %d'%(split,len(self.datapath)))

        self.cache_size = cache_size  # how many data points to cache in memory
        self.cache = {}  # from index to (point_set, cls) tuple

        data = []
        labels = []
        for index in range(len(self.datapath)):
            path_and_index = self.datapath[index]
            path_and_index = path_and_index.split(' ')
            path = path_and_index[0]
            label = int(path_and_index[1])
            point_set = np.loadtxt(path, delimiter=' ').astype(np.float32) #[N,3+4+20+2]
            point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

            if self.use_ph:
                npoints = point_set.shape[0]
                table = np.tile(self.ph_table,[npoints,1,1])
                res_f = point_set[:,7:27]
                ph_f = table[res_f==1]
                a = point_set[:,0:27]
                b = point_set[:,[27,28]]
                point_set = np.concatenate((a,ph_f,b),axis=1) #29+6=35
            
            data.append(point_set)
            labels.append(label)

        self.data = data
        self.labels = labels

    def get_labels(self): return self.labels

    def get_size(self):
        return len(self.datapath)

    def __len__(self):
        return len(self.datapath)

    def _get_item(self, index):
        
        
        # if index in self.cache:
        #     point_set, label = self.cache[index]
        # else:
        #     path_and_index = self.datapath[index].path_and_index.split('')
        #     path = path_and_index[0]
        #     label = path_and_index.split[1]
        #     #print(path)
        #     #print(label)
        #     #label = self.classes[self.datapath[index][0]]
        #     #label = np.array([label]).astype(np.int32)
        #     point_set = np.loadtxt(path, delimiter=' ').astype(np.float32) #[N,3+4+20+2]

        #     point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

        #     if self.use_ph:
        #         npoints = point_set.shape[0]
        #         table = np.tile(self.ph_table,[npoints,1,1])
        #         res_f = point_set[:,7:27]
        #         ph_f = table[res_f==1]
        #         a = point_set[:,0:27]
        #         b = point_set[:,[27,28]]
        #         point_set = np.concatenate((a,ph_f,b),axis=1) #29+6=35
            #print(p.shape)

            #if point_set.shape[0]!=500:
            #    print(path)
            #    print(point_set.shape[0])
            #    print('lig',point_set[point_set[:,-1]==1].shape[0])
            #    print('rec',point_set[point_set[:,-1]==0].shape[0])
            #print(point_set.shape)
            #FPS
            
            
            #point_set = point_set[:,[0,1,2,3,4,5,6,27,28]]
            
            #R= point_set[point[:,-1]==0]
            #L= point_set[point[:,-1]==1]

            #if point_set.shape[0]>self.npoints:
            #    point_set = interface_nearest_point_sample(point_set, self.npoints)

            '''
            f_num = point_set.shape[1]

            if point_set.shape[0]>self.npoints:
                if self.uniform:
                    point_set = farthest_point_sample(point_set, self.npoints)
                else:
                    point_set = point_set[0:self.npoints,:]
                point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])
            else:
                point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])
                padding = np.zeros((self.npoints-point_set.shape[0],f_num))
                padding = padding.astype(np.float32)
                point_set = np.concatenate((point_set,padding))

            #point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])
            point_set = point_set[:,[0,1,2,3,4,5,6,27,28]]
            
            if not self.normal_channel:
                point_set = point_set[:, 0:3]

            '''

            # if len(self.cache) < self.cache_size:
            #     self.cache[index] = (point_set, label)

    def __getitem__(self, index):
        point_set = self.data[index]
        label = self.labels[index]

        return point_set,label

class DataLoaderForComplexCV(Dataset):
    def __init__(self,root,input_complex,all_data_path,use_ph=False):
        self.complex = input_complex
        self.all_data_path = all_data_path
        self.root = root

        self.use_ph =use_ph
        if self.use_ph:
            ph_table = pd.read_csv(os.path.join(self.root, 'physicochemical_class_onehot.csv'),index_col=0)
            self.ph_table = ph_table.values
        
        #print(complex)
        #print(all_data_path)
        data_path = []
        complex_names = set()

        for dp in self.all_data_path:
            complex_name = dp.split('/')[0]
            if (complex_name[:4] in self.complex):
                data_path.append(os.path.join(self.root,dp))
                complex_names.add(complex_name)
        
        self.complex = list(complex_names)

        self.datapath = data_path

        data = []
        labels = []
        for index in range(len(self.datapath)):
            path_and_index = self.datapath[index]
            path_and_index = path_and_index.split(' ')
            path = path_and_index[0]
            label = int(path_and_index[1])
            point_set = np.loadtxt(path, delimiter=' ').astype(np.float32) #[N,3+4+20+2]
            point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

            if self.use_ph:
                npoints = point_set.shape[0]
                table = np.tile(self.ph_table,[npoints,1,1])
                res_f = point_set[:,7:27]
                ph_f = table[res_f==1]
                a = point_set[:,0:27]
                b = point_set[:,[27,28]]
                point_set = np.concatenate((a,ph_f,b),axis=1) #29+6=35
            
            data.append(point_set)
            labels.append(label)

        self.data = data
        self.labels = labels
    def get_labels(self): return self.labels

    def get_size(self):
        return len(self.data)

    def __len__(self):
        return len(self.data)

    def get_complex_names(self):
        return self.complex

    def __getitem__(self, index):
        point_set = self.data[index]
        label = self.labels[index]

        return point_set,label

class PPI4DOCKDataLoaderCV(Dataset):
    def __init__(self, data,labels):
        self.data = data
        self.labels = labels
    def get_labels(self): return self.labels

    def get_size(self):
        return len(self.data)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        point_set = self.data[index]
        label = self.labels[index]

        return point_set,label

def get_kflod_complex(k,i,X):
    fold_size = len(X) // k
    val_start = i * fold_size
    if i != k - 1:
        val_end = (i + 1) * fold_size
        X_valid = X[val_start:val_end]
        X_train =  X[0:val_start]+X[val_end:]
    else:
        X_valid = X[val_start:]
        X_train = X[0:val_start]

    return X_train,X_valid

def get_kfold_data(k, i, X, y):  
    fold_size = len(X) // k  
    
    val_start = i * fold_size
    if i != k - 1:
        val_end = (i + 1) * fold_size
        X_valid, y_valid = X[val_start:val_end], y[val_start:val_end]
        X_train = X[0:val_start]+X[val_end:]
        y_train = y[0:val_start]+y[val_end:]
    else:
        X_valid, y_valid = X[val_start:], y[val_start:]     
        X_train = X[0:val_start]
        y_train = y[0:val_start]
        
    return X_train, y_train, X_valid,y_valid

def split_train_valid(data,train=0.9):
    import random
    random.seed(512)
    random.shuffle(data)
    train_size = int(train*len(data))
    train_set = data[:train_size]
    valid_set = data[train_size:len(data)]
    return train_set,valid_set

class PPI4DOCKDataLoaderAll(Dataset):
    def __init__(self, root,  npoint=720, uniform=False, normal_channel=True, cache_size=15000,use_ph=False):
        self.root = root
        self.npoints = npoint
        self.uniform = uniform
        self.catfile = os.path.join(self.root, 'class.txt')
        self.use_ph =use_ph
        if self.use_ph:
            ph_table = pd.read_csv(os.path.join(self.root, 'physicochemical_class_onehot.csv'),index_col=0)
            self.ph_table = ph_table.values

        self.cat = [line.rstrip() for line in open(self.catfile)]
        self.classes = dict(zip(self.cat, range(len(self.cat))))
        #print(self.classes['postive'])
        self.normal_channel = normal_channel

        if not os.path.exists(os.path.join(self.root, 'all.txt')):

            data_paths = {}
            #data_paths['train'] = [line.rstrip() for line in open(os.path.join(self.root, 'train.txt'))]
            #data_paths['valid'] = [line.rstrip() for line in open(os.path.join(self.root, 'valid.txt'))]
            #data_paths['test'] = [line.rstrip() for line in open(os.path.join(self.root, 'test.txt'))]

            data_paths['train'] = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'train.txt'))]
            data_paths['valid'] = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'valid.txt'))]
            data_paths['test'] = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'test.txt'))]

            data_path = data_paths['train']+data_paths['valid']+data_paths['test']
            self.datapath = data_path
        else:
            data_path = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, 'all.txt'))]
            self.datapath = data_path
        print('The size of all data is %d'%(len(self.datapath)))

        self.cache_size = cache_size  # how many data points to cache in memory
        self.cache = {}  # from index to (point_set, cls) tuple

        data = []
        labels = []
        for index in range(len(self.datapath)):
            path_and_index = self.datapath[index]
            path_and_index = path_and_index.split(' ')
            path = path_and_index[0]
            label = int(path_and_index[1])
            point_set = np.loadtxt(path, delimiter=' ').astype(np.float32) #[N,3+4+20+2]
            point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

            if self.use_ph:
                npoints = point_set.shape[0]
                table = np.tile(self.ph_table,[npoints,1,1])
                res_f = point_set[:,7:27]
                ph_f = table[res_f==1]
                a = point_set[:,0:27]
                b = point_set[:,[27,28]]
                point_set = np.concatenate((a,ph_f,b),axis=1) #29+6=35
            
            data.append(point_set)
            labels.append(label)

        self.data = data
        self.labels = labels

    def get_labels(self): return self.labels

    def get_size(self):
        return len(self.datapath)

    def __len__(self):
        return len(self.datapath)

    def _get_item(self, index):
        pass

    def get_cv_dataloader(self,k,i):
        X_train, y_train, X_test,y_test = get_kfold_data(k,i,self.data,self.labels)
        test_dataloader = PPI4DOCKDataLoaderCV(X_test,y_test)

        X_train,X_valid = split_train_valid(X_train)
        y_train,y_valid = split_train_valid(y_train)

        train_dataloader = PPI4DOCKDataLoaderCV(X_train,y_train)
        valid_dataloader = PPI4DOCKDataLoaderCV(X_valid,y_valid)

        print('In fold {}/{} ,The size of train data is {}'.format(i,k,len(X_train)))
        print('In fold {}/{} ,The size of valid data is {}'.format(i,k,len(X_valid)))
        print('In fold {}/{} ,The size of test data is {}'.format(i,k,len(X_test)))

        return train_dataloader,valid_dataloader,test_dataloader

    def __getitem__(self, index):
        point_set = self.data[index]
        label = self.labels[index]

        return point_set,label

class DataLoaderForComplex(Dataset):
    def __init__(self, root,  npoint=720, uniform=False, normal_channel=True, cache_size=15000,use_ph=False):
        self.root = root
        self.npoints = npoint
        self.uniform = uniform
        self.catfile = os.path.join(self.root, 'class.txt')
        self.use_ph =use_ph
        if self.use_ph:
            ph_table = pd.read_csv(os.path.join(self.root, 'physicochemical_class_onehot.csv'),index_col=0)
            self.ph_table = ph_table.values

        self.cat = [line.rstrip() for line in open(self.catfile)]
        self.classes = dict(zip(self.cat, range(len(self.cat))))
        #print(self.classes['postive'])
        self.normal_channel = normal_channel
        all_complex_unique = []

        if not os.path.exists(os.path.join(self.root, 'all.txt')):

            all_complex = {}
            data_paths = {}

            data_paths['train'] = [line.rstrip() for line in open(os.path.join(self.root, 'train.txt'))]
            data_paths['valid'] = [line.rstrip() for line in open(os.path.join(self.root, 'valid.txt'))]
            data_paths['test'] = [line.rstrip() for line in open(os.path.join(self.root, 'test.txt'))]

            data_path = data_paths['train']+data_paths['valid']+data_paths['test']
            self.datapath = data_path
            

            all_complex['train'] = [line.split('/')[0] for line in open(os.path.join(self.root, 'train.txt'))]
            all_complex['valid'] = [line.split('/')[0] for line in open(os.path.join(self.root, 'valid.txt'))]
            all_complex['test'] = [line.split('/')[0] for line in open(os.path.join(self.root, 'test.txt'))]
            complex = all_complex['train']+all_complex['valid']+all_complex['test']
            
            for c in complex:
                if c not in all_complex_unique:
                    all_complex_unique.append(c)
            self.all_complex_unique = all_complex_unique
        else:
            data_path = [line.rstrip() for line in open(os.path.join(self.root, 'all.txt'))]
            complex = [line.split('/')[0] for line in open(os.path.join(self.root, 'all.txt'))]

            self.datapath = data_path
            for c in complex:
                if c not in all_complex_unique:
                    all_complex_unique.append(c)
            self.all_complex_unique = all_complex_unique

        print('The size of all data is %d and have %d complex'%(len(self.datapath),len(self.all_complex_unique)))

        self.cache_size = cache_size  # how many data points to cache in memory
        self.cache = {}  # from index to (point_set, cls) tuple

    def get_labels(self): return self.labels

    def get_size(self):
        return len(self.datapath)

    def __len__(self):
        return len(self.datapath)

    def _get_item(self, index):
        pass
    
    def get_cv_dataloader_use_txt(self,txt_dir,i):
        complexs = []
        with open(txt_dir) as f:
            line = f.readline().strip()
            while line:
                #line = line.lower()
                complexs.append(line.split(','))
                line = f.readline().strip()
        k = len(complexs)
        complex_test= complexs[i]
        complex_train = []
        for j in range(k):
            if j!=i:
                complex_train+=complexs[j]

        print(complex_test)
        
        test_dataloader = DataLoaderForComplexCV(self.root,complex_test,self.datapath,self.use_ph)
        complex_train,complex_valid= split_train_valid(complex_train)

        print(complex_train)
        print(complex_valid)

        train_dataloader = DataLoaderForComplexCV(self.root,complex_train,self.datapath,self.use_ph)
        valid_dataloader = DataLoaderForComplexCV(self.root,complex_valid,self.datapath,self.use_ph)

        print('In fold {}/{} ,The size of train size is {} and have {} complex'.format(i,k,train_dataloader.get_size(),len(complex_train)))
        print('In fold {}/{} ,The size of valid size is {} and have {} complex'.format(i,k,valid_dataloader.get_size(),len(complex_valid)))
        print('In fold {}/{} ,The size of test size is {} and have {} complex'.format(i,k,test_dataloader.get_size(),len(complex_test)))

        return train_dataloader,valid_dataloader,test_dataloader


    def get_cv_dataloader(self,k,i):
        complex_train, complex_test = get_kflod_complex(k,i,self.all_complex_unique)
        test_dataloader = DataLoaderForComplexCV(self.root,complex_test,self.datapath,self.use_ph)

        complex_train,complex_valid= split_train_valid(complex_train)

        train_dataloader = DataLoaderForComplexCV(self.root,complex_train,self.datapath,self.use_ph)
        valid_dataloader = DataLoaderForComplexCV(self.root,complex_valid,self.datapath,self.use_ph)

        print('In fold {}/{} ,The size of train size is {} and have {} complex'.format(i,k,train_dataloader.get_size(),len(complex_train)))
        print('In fold {}/{} ,The size of valid size is {} and have {} complex'.format(i,k,valid_dataloader.get_size(),len(complex_valid)))
        print('In fold {}/{} ,The size of test size is {} and have {} complex'.format(i,k,test_dataloader.get_size(),len(complex_test)))

        return train_dataloader,valid_dataloader,test_dataloader

    def __getitem__(self, index):
        point_set = self.data[index]
        label = self.labels[index]

        return point_set,label

class DataLoaderForComplexUseHit(Dataset):
    def __init__(self, root,  npoint=500, split='hit_test', uniform=False, normal_channel=True, cache_size=15000,use_res=True,use_ph=False):
        self.root = root
        print(self.root)
        self.npoints = npoint
        self.uniform = uniform
        self.normal_channel = normal_channel
        self.use_res = use_res
        self.use_ph =use_ph
        if self.use_ph:
            ph_table = pd.read_csv(os.path.join(self.root,'../','physicochemical_class_onehot.csv'),index_col=0)
            self.ph_table = ph_table.values

        assert (split == 'hit_test')
        self.file_list = os.listdir(self.root)
        self.datapath = [os.path.join(self.root,line.rstrip()) for line in self.file_list]
        
        print('The size of %s data is %d'%(split,len(self.datapath)))

        self.cache_size = cache_size  # how many data points to cache in memory
        self.cache = {}  # from index to (point_set, cls) tuple
    
    def get_size(self):
        return len(self.datapath)

    def __len__(self):
        return len(self.datapath)

    def _get_item(self, index):
        if index in self.cache:
            point_set,label = self.cache[index]
        else:
            label = -1
            path = self.datapath[index]
            point_set = np.loadtxt(path, delimiter=' ').astype(np.float32)
            point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

            if self.use_ph:
                npoints = point_set.shape[0]
                table = np.tile(self.ph_table,[npoints,1,1])
                res_f = point_set[:,7:27]
                ph_f = table[res_f==1]
                a = point_set[:,0:27]
                b = point_set[:,[27,28]]
                point_set = np.concatenate((a,ph_f,b),axis=1) #29+6=35

            if not self.use_res:
                point_set = point_set[:,[0,1,2,3,4,5,6,27,28]]

            if len(self.cache) < self.cache_size:
                self.cache[index] = point_set,label

        return point_set,label,self.datapath[index]

    def __getitem__(self, index):
        return self._get_item(index)

class PPI4DOCKDataLoader_Complex(Dataset):
    def __init__(self, root,  npoint=1000, split='train', uniform=False, normal_channel=True, cache_size=15000,use_ph=False):
        self.root = root
        self.npoints = npoint
        self.uniform = uniform
        self.catfile = os.path.join(self.root, 'class.txt')
        self.use_ph =use_ph
        if self.use_ph:
            ph_table = pd.read_csv(os.path.join(self.root, 'physicochemical_class_onehot.csv'),index_col=0)
            self.ph_table = ph_table.values

        self.cat = [line.rstrip() for line in open(self.catfile)]
        self.classes = dict(zip(self.cat, range(len(self.cat))))
        #print(self.classes['postive'])
        self.normal_channel = normal_channel

        self.datapath = [os.path.join(self.root,line.rstrip()) for line in open(os.path.join(self.root, '{}.txt'.format(split)))]
        assert (split == 'train' or split == 'valid' or split == 'test')

        data_complex = []
        labels_complex = []
        pdb_names = []
        
        last_pdb_name = ''
        data = []
        labels = []
        for index in range(len(self.datapath)):
            path_and_index = self.datapath[index]
            path_and_index = path_and_index.split(' ')
            path = path_and_index[0]
            pdb_name = path.split('/')[0].split('\\')[-1]

            # New Complex
            if last_pdb_name !=pdb_name:
                if len(data)>0:
                    pdb_names.append(last_pdb_name)
                    data_complex.append(data)
                    labels_complex.append(labels)
                    data=[]
                    labels = []
                    last_pdb_name = pdb_name
                else:
                    last_pdb_name = pdb_name

            label = int(path_and_index[1])
            point_set = np.loadtxt(path, delimiter=' ').astype(np.float32) #[N,3+4+20+2]
            point_set[:, 0:3] = pc_normalize(point_set[:, 0:3])

            if self.use_ph:
                npoints = point_set.shape[0]
                table = np.tile(self.ph_table,[npoints,1,1])
                res_f = point_set[:,7:27]
                ph_f = table[res_f==1]
                a = point_set[:,0:27]
                b = point_set[:,[27,28]]
                point_set = np.concatenate((a,ph_f,b),axis=1) #29+6=35
            
            data.append(point_set)
            labels.append(label)
        
        pdb_names.append(last_pdb_name)
        data_complex.append(data)
        labels_complex.append(labels)

        self.data = data_complex
        self.labels = labels_complex
        self.complex_names = pdb_names

        print('The size of %s data is %d and have %d complex'%(split,len(self.datapath),len(self.data)))

    def get_labels(self): return self.labels

    def get_size(self):
        return len(self.data)
    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        point_sets = self.data[index]
        labels = self.labels[index]
        names = self.complex_names[index]

        return point_sets,labels,names

class PPI4DOCKDataLoader_ComplexForHit(Dataset):
    def __init__(self,data,labels):
        self.data = data
        self.labels = labels
    
    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        point_set = np.squeeze(self.data[index])
        label = self.labels[index]
        return point_set,label

def collate_fn(batch):
    f_num = batch[0][0].shape[1]
    max_natoms = 1000 #max([item[0].shape[0] for item in batch if item is not None])

    #print(max_natoms)

    labels = []
    point_set = np.zeros((len(batch), max_natoms, f_num))

    for i in range(len(batch)):
        item = batch[i]
        num_atom = item[0].shape[0]
        point_set[i,:num_atom] = item[0]
        labels.append(item[1])
    
    labels = np.array(labels)

    return point_set,labels,max_natoms


def collate_fn_hit(batch):

    f_num = batch[0][0].shape[1]
    max_natoms = 1000 #max([item[0].shape[0] for item in batch if item is not None])

    #print(max_natoms)

    labels = []
    paths = []
    point_set = np.zeros((len(batch), max_natoms, f_num))

    for i in range(len(batch)):
        item = batch[i]
        num_atom = item[0].shape[0]
        point_set[i,:num_atom] = item[0]
        labels.append(item[1])
        paths.append(item[2])
    
    labels = np.array(labels)

    return point_set,labels,max_natoms,paths

#train 2668 test 297
if __name__ == '__main__':
    import torch

    data_all = DataLoaderForComplex('C:\\Users\\cxhrzh\\Desktop\\PointDock\\Data\\dockground61_1000', npoint=1000,normal_channel=True)
    TRAIN_DATASET ,VALID_DATASET ,TEST_DATASET = data_all.get_cv_dataloader_use_txt('C:\\Users\\cxhrzh\\Desktop\\code\\set.txt',1)
    print(TRAIN_DATASET.get_complex_names())
    # DataLoader = torch.utils.data.DataLoader(TEST_DATASET,batch_size=1, shuffle=True)
    # for data_complex,label_complex,name in DataLoader:
    #     data = PPI4DOCKDataLoader_ComplexForHit(data_complex,label_complex)
    #     dl = torch.utils.data.DataLoader(data,batch_size=4, shuffle=False,collate_fn=collate_fn)
    #     for point,label,num in dl:
    #         print(point.shape)
    #         print(label.shape)
        #if point.shape[0]!=250:
            #print(point.shape)
        # protein, tai , label
        # [batch,number_res,shape],[batch,number_res,shape],[batch]