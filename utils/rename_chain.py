import os
import pandas as pd
import numpy as np
import shutil
import operator
from functools import reduce


#change chain 
def rename_chian_id_by_name(source_path, target_path, chain1_list, chain2_list, r_chain1, r_chain2):
    lines = []
    with open(source_path,'r') as f:
        line = f.readline().strip()
        while line:
            if line.startswith('ATOM'):
                if line[21] in chain1_list:
                    line = line[:21]+r_chain1+line[22:]
                    lines.append(line)
                elif line[21] in chain2_list:
                    line = line[:21]+r_chain2+line[22:]
                    lines.append(line)
                line = f.readline().strip()
            else:
                lines.append(line)
                line = f.readline().strip()
    with open(target_path,'w+') as f:
        for line in lines:
            f.write(line+'\n')



def rename_pdb_file_by_name(input_path,sv_path):
    if not os.path.exists(sv_path):
        os.mkdir(sv_path)
    listfiles=[x for x in os.listdir(input_path) if ".pdb" in x]
    listfiles.sort()
    for item in listfiles:
        input_pdb_path=os.path.join(input_path,item)
        target_path = os.path.join(sv_path,item)
        r_chain1 = 'A'
        r_chain2 = 'B'
        chain1_list = ['H','L']
        chain2_list = ['Z']
        rename_chian_id_by_name(input_pdb_path,target_path,chain1_list,chain2_list,r_chain1,r_chain2)


def rename_chian_id_by_linenum(source_path,target_path,r_chain1, r_chain2,r_number):
    lines = []
    # print(r_number)
    with open(source_path,'r') as f:
        num=0
        for line in f.readlines():
            line = line.strip()
            # print(line)
            num+=1
            if line.startswith('ATOM'):
                if num<=r_number:
                    line = line[:21]+r_chain1+line[22:]
                    lines.append(line)
                else:
                    line = line[:21]+r_chain2+line[22:]
                    lines.append(line)
                # line = f.readline().strip()
            else:
                lines.append(line)
                # line = f.readline().strip()
    with open(target_path,'w+') as f:
        for line in lines:
            f.write(line+'\n')

def rename_chain_id_pdb_file_by_line(input_path,output_path,native_docking_path):
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    
    for pdb_id in os.listdir(input_path):
        print(pdb_id)
        origin_pdb_folder = os.path.join(input_path,pdb_id)
        rename_pdb_folder = os.path.join(output_path,pdb_id)
        if not os.path.exists(rename_pdb_folder):
            os.mkdir(rename_pdb_folder)
        rec_file_path = os.path.join(native_docking_path,'{}_r_u.pdb.ms'.format(pdb_id))
        rec_number=0
        with open(rec_file_path,'r') as f:
            lines = f.readlines()
            rec_number = len(lines)
        case_list=[x for x in os.listdir(origin_pdb_folder) if ".pdb" in x]
        case_list.sort()
        for case in case_list:
            input_case_path=os.path.join(origin_pdb_folder,case)
            target_case_path = os.path.join(rename_pdb_folder,case)
            rename_chian_id_by_linenum(input_case_path,target_case_path,'A','B',rec_number)

if __name__ == '__main__':
    input_path = 'D:\\share\\BM44\\BM44_decoys'
    output_path = 'D:\share\BM44\BM44_decoys_AB'
    native_docking_path = 'D:\\share\\zdock_preprocess\\decoys_bm4_zd3.0.2_6deg\\input_pdbs'

    # input_path = '/home/mxp/chenzihao/BM44_decoys'
    # output_path = '/home/mxp/chenzihao/BM44_decoys_AB'
    # native_docking_path = '/home/mxp/chenzihao/input_pdbs'
    

    rename_chain_id_pdb_file_by_line(input_path,output_path,native_docking_path)

    # input_path = 'C:\\Users\\cxhrzh\\Desktop\\code\\\kangti'
    # output_path = os.path.join('C:\\Users\\cxhrzh\\Desktop\\code\\','relax')
    
    # if not os.path.exists(output_path):
    #     os.mkdir(output_path)

    # pdbs = os.listdir(input_path)
    # pdbs = ['1A2Y']
    # #print(pdbs)
    # for pdb in pdbs:
    #     in_path = os.path.join(input_path,pdb)
    #     out_path= os.path.join(output_path,pdb)
    #     rename_pdb_file(in_path,out_path,pdb)